'use strict';

function createElement(node) {

  if ((typeof node === 'string')) {
    return document.createTextNode(node.toString());
  }

  if (Array.isArray(node)) {
    return node.reduce((f, elem) => {
      f.appendChild(createElement(elem));
      return f;
    }, document.createDocumentFragment());
  }

  const element = document.createElement(node.name);

  // [].concat(node.cls || []).forEach(
  //     className => element.classList.add(className)
  // );

  if (node.props) {
    Object.keys(node.props).forEach(
      key => element.setAttribute(key, node.props[key])
    );
  }

  if (node.childs) {
    element.appendChild(createElement(node.childs))
  };

  return element;
}