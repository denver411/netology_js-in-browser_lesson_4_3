'use strict'

document.addEventListener('DOMContentLoaded', () => {

  const seatsQuantity = {
    adultsSeats: 0,
    kidsSeats: 0,
    get totalSeats() {
      return this.adultsSeats + this.kidsSeats
    }
  }

  const btnSetFull = document.getElementById('btnSetFull');
  btnSetFull.setAttribute('disabled', '');

  const btnSetEmpty = document.getElementById('btnSetEmpty');
  btnSetEmpty.setAttribute('disabled', '');

  btnSetFull.addEventListener('click', event => {
    event.preventDefault();
    Array.from(document.getElementsByClassName('seat')).forEach(item => {
      if (item.classList.contains('adult')) {
        item.classList.remove('adult');
      } else if (item.classList.contains('half')) {
        item.classList.remove('half');
      }
      item.classList.add('adult');
    });
    seatQuantityMonitor();
  })

  btnSetEmpty.addEventListener('click', event => {
    event.preventDefault();
    Array.from(document.getElementsByClassName('seat')).forEach(item => {
      if (item.classList.contains('adult')) {
        item.classList.remove('adult');
      } else if (item.classList.contains('half')) {
        item.classList.remove('half');
      }
    });
    seatQuantityMonitor();
  })


  document.getElementById('btnSeatMap').addEventListener('click', (event) => {

    event.preventDefault();

    const planeId = document.getElementById('acSelect').value;

    fetch(`https://neto-api.herokuapp.com/plane/${planeId}`)
      .then(res => res.json())
      .then(createContent)

  })

  function createContent(data) {

    const planeName = document.getElementById('seatMapTitle');
    planeName.innerText = `${data.title} (${data.passengers} пассажиров)`

    btnSetFull.removeAttribute('disabled');
    btnSetEmpty.removeAttribute('disabled');

    const container = document.getElementById('seatMapDiv');

    Array.from(container.children).forEach(item => {
      container.removeChild(item);
    })

    data.scheme.forEach((row, index) => {

      const seatingRow = document.createElement('div');
      seatingRow.className = 'row seating-row text-center';

      const rowNumber = document.createElement('div');
      rowNumber.className = 'col-xs-1 row-number';

      const rowNumberTitle = document.createElement('h2');
      rowNumberTitle.className = '';

      const rowLeftPart = document.createElement('div');
      rowLeftPart.className = 'col-xs-5';

      let rowRightPart = rowLeftPart.cloneNode();

      let seat = document.createElement('div');
      seat.className = 'col-xs-4';
      let seatNumber = document.createElement('span');
      seatNumber.className = 'seat-label';
      seat.appendChild(seatNumber);

      container.appendChild(seatingRow);
      seatingRow.appendChild(rowNumber);
      rowNumberTitle.innerText = index + 1;
      rowNumber.appendChild(rowNumberTitle);
      seatingRow.appendChild(rowLeftPart);
      seatingRow.appendChild(rowRightPart);

      let seatType = '';
      row ? seatType = 'seat' : seatType = 'no-seat';

      seat.classList.add(seatType);

      for (let i = 0; i < 3; i++) {
        rowLeftPart.appendChild(seat.cloneNode(true));
        if (rowLeftPart.lastChild.className === 'col-xs-4 seat') {
          rowLeftPart.lastChild.lastChild.innerText = data.letters6[i];
        }
        rowRightPart.appendChild(seat.cloneNode(true));
        if (rowRightPart.lastChild.className === 'col-xs-4 seat') {
          rowRightPart.lastChild.lastChild.innerText = data.letters6[i + 3];
        }
      }

      if (row === 4) {
        rowLeftPart.firstChild.className = 'col-xs-4 no-seat';
        rowLeftPart.firstChild.firstChild.innerText = '';
        rowRightPart.lastChild.className = 'col-xs-4 no-seat';
        rowRightPart.lastChild.firstChild.innerText = '';
      }

    })

    Array.from(document.getElementsByClassName('seat')).forEach(item => item.addEventListener('click', chooseSeat));

    seatQuantityMonitor();
  }

  function chooseSeat(event) {

    event.stopPropagation();

    if (event.currentTarget.classList.contains('adult')) {
      event.currentTarget.classList.remove('adult');
    } else if (event.currentTarget.classList.contains('half')) {
      event.currentTarget.classList.remove('half');
    } else if (event.altKey) {
      event.currentTarget.classList.add('half');
    } else {
      event.currentTarget.classList.add('adult');
    }

    seatQuantityMonitor();
  }

  function seatQuantityMonitor() {

    const totalPax = document.getElementById('totalPax');
    const totalAdult = document.getElementById('totalAdult');
    const totalHalf = document.getElementById('totalHalf');

    seatsQuantity.adultsSeats = Array.from(document.getElementsByClassName('adult')).length;
    seatsQuantity.kidsSeats = Array.from(document.getElementsByClassName('half')).length;
    // seatsQuantity.totalSeats = seatsQuantity.adultsSeats + seatsQuantity.kidsSeats;

    totalPax.innerText = seatsQuantity.totalSeats;
    totalAdult.innerText = seatsQuantity.adultsSeats;
    totalHalf.innerText = seatsQuantity.kidsSeats;
  }

})